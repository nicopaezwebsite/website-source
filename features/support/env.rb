require File.expand_path(File.dirname(__FILE__) + "/../../config/boot")

require 'capybara/cucumber'
require 'rspec/expectations'
require 'rspec/expectations'
require 'rspec'
require 'rack/test'
require 'simplecov'

include Rack::Test::Methods

SimpleCov.start do
  root(File.join(File.dirname(__FILE__), '..','..'))
  coverage_dir 'reports/coverage'
  add_filter '/spec/'
  add_filter '/features/'
  add_filter '/admin/'
  add_filter '/db/'
  add_filter '/config/'
  add_group "Models", "app/models"
  add_group "Controllers", "app/controllers"
  add_group "Helpers", "app/helpers"
end

DataMapper.auto_migrate!
DataMapper::Logger.new($stdout, :all)

def app
  NicoPaez::App
end

puts ENV['RACK_ENV']

ENV['redirection_mode'] = 'show'
Capybara.app = NicoPaez::App.tap { | app | }