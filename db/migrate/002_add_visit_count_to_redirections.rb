migration 2, :add_visit_count_to_redirections do
  up do
    modify_table :redirections do
      add_column :visit_count, Integer
    end
  end

  down do
    modify_table :redirections do
      drop_column :visit_count
    end
  end

end