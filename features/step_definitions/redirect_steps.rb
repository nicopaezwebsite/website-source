Given(/^I define the key "(.*?)" associated with "(.*?)"$/) do |key, url|
  visit '/newlink'
  fill_in('redirection[target_url]', :with => url)
  fill_in('redirection[keyword]', :with => key)
  click_button('Generate')
  page.status_code.should be 200
end

When(/^browse "(.*?)"$/) do |key|
  visit "#{key}"
end

Then(/^I should get "(.*?)"$/) do |url|
  page.should have_content(url)
end

Given(/^there is no some_stuff key defined$/) do
  #nothing to do here
end

Then(/^I should get the version of the app$/) do
  page.should have_content(Version.current)
end

Then(/^visit count for "(.*?)" should be (\d+)$/) do |keyword, count|
  r = Redirection.get_by_keyword(keyword)
  expect(r.visit_count).to eq count.to_i
end

When(/^browse "(.*?)" (\d+) times$/) do |target_url, count|
  count.to_i.times { visit "#{target_url}" }
end
