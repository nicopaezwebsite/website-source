

class Redirection
	include DataMapper::Resource

	property :id, Serial
	property :keyword, String
	property :target_url, String
	property :visit_count, Integer, :default => 0
	
	def self.get_by_keyword(a_keyword)
		r = self.first(:keyword => a_keyword.downcase)
		r
	end

	def self.create_for(shorcut, url)
		r = Redirection.new
		r.keyword = shorcut.downcase
		r.target_url = url.downcase
		r.visit_count = 0
		return r
	end

	def register_visit
		if self.visit_count.nil?
			self.visit_count = 0
		end
		self.visit_count += 1
	end

end