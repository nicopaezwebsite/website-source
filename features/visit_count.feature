Feature: Visit count
In order to measure site sucess
I want to count visit

Scenario: First visit should count 1
	Given I define the key "scrum" associated with "http://scrum.org"
	When browse "/scrum"
	Then visit count for "scrum" should be 1

Scenario: Each visit should increment count in 1
	Given I define the key "xp" associated with "http://xp.org"
	Given I define the key "crystal" associated with "http://crysal.org"
	When browse "/xp" 3 times
	Then visit count for "xp" should be 3
	And visit count for "crystal" should be 0