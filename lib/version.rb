class Version

  MAYOR = 5
  MINOR = 0
  PATCH = 2

  def self.current
    "#{MAYOR}.#{MINOR}.#{PATCH}-#{ENV['HEROKU_SLUG_COMMIT']}"
  end

end