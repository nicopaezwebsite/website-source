require_relative '../spec_helper.rb'
require 'byebug'

describe Redirection do
	
	describe 'model' do 

		subject { @redirection = Redirection.new }
		it { should respond_to :id }
		it { should respond_to :keyword }
		it { should respond_to :target_url }
		it { should respond_to :visit_count }
		
	end

	describe 'create' do 
		it 'should return a normalized instance' do
			r = Redirection.create_for('A_ShoRt_NAME', 'the_LONG_urL.cOm')
			expect(r.keyword).to eq 'a_short_name'
			expect(r.target_url).to eq 'the_long_url.com'
		end

		it 'should initilize visit_count in zero' do
			r = Redirection.create_for('A_ShoRt_NAME', 'the_LONG_urL.cOm')
			expect(r.visit_count).to eq 0
		end
	end

	describe 'get_by_keyword' do
		it 'should search no matter casing' do
			Redirection.should_receive(:first).with(:keyword => 'some_keyword').and_return(nil)
			Redirection.get_by_keyword('soME_kEyworD')
		end
	end

	describe 'register_visit' do
		it 'should increment visit count' do
			r = Redirection.create_for('short_name', 'http://very_long.url')
			r.register_visit
			expect(r.visit_count).to be 1
		end

		it 'should initializar visit count' do
			r = Redirection.create_for('short_name', 'http://very_long.url')
			r.visit_count = nil
			r.register_visit
			expect(r.visit_count).to be 1
		end

	end
	
end