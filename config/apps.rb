Padrino.configure_apps do
	set :session_secret, ENV['session_secret']	
  set :protection, true
  set :protect_from_csrf, true
end

Padrino.mount('NicoPaez::App', :app_file => Padrino.root('app/app.rb')).to('/')
