module NicoPaez
  class App < Padrino::Application
    enable :sessions
    register Padrino::Rendering
    register Padrino::Helpers
    include Recaptcha::ClientHelper
    include Recaptcha::Verify
    
    Recaptcha.configure do |config|
      config.public_key  = ENV['RECAPTCHA_SITE_KEY']
      config.private_key = ENV['RECAPTCHA_SECRET_KEY']
    end

    get '/' do
      render :index
    end

    get '/meet' do
      render :meet
    end

    get '/newlink' do
      @redirection = Redirection.new
      render :new_link
    end

    post '/newlink' do
      if (verify_recaptcha || (ENV['PADRINO_ENV'] == 'test'))
        r = Redirection.new(params['redirection'])
        if r.save 
          flash[:success] = 'Redirection created'
        else
          return 503, r.errors
        end
      else
        flash[:error] = 'Not valid'
      end 
      @redirection = Redirection.new   
      render :new_link
    end

    get '/health' do
      status = { :version => Version.current }
      status.to_json
    end

    get '/version' do
      version = { :version => Version.current }.to_json
    end

    get ':keyword' do
      r = Redirection.get_by_keyword(params[:keyword])
      if r.nil?
        status 404
        render :not_found
      else
        r.register_visit
        r.save!
        if ENV['redirection_mode'].nil?
          redirect to r.target_url
        else
          return 200, r.target_url
        end
      end
    end

  end
end
