migration 1, :create_redirections do
  up do
    create_table :redirections do
      column :id, Integer, :serial => true
      column :keyword, DataMapper::Property::String, :length => 255
      column :target_url, DataMapper::Property::String, :length => 255
    end
  end

  down do
    drop_table :redirections
  end
end
