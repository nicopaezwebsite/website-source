Feature: Browse redirects
In order to recommend web resources
I want to browse a key and be redirect to the associates resource

Scenario: One word key redirect
	Given I define the key "scrumx" associated with "http://scrum.org"
	When browse "/scrumx"
	Then I should get "http://scrum.org"

Scenario: Not found
	Given there is no some_stuff key defined
	When browse "/some_stuff"
	Then I should get "Oooops! Not found"	
